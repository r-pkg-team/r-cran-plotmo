r-cran-plotmo (3.6.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Wed, 11 Sep 2024 15:57:57 +0900

r-cran-plotmo (3.6.3-1) unstable; urgency=medium

  [ Charles Plessy ]
  * Team upload
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

  [ Andreas Tille ]
  * Disable reprotest

 -- Charles Plessy <plessy@debian.org>  Tue, 14 May 2024 08:52:16 +0900

r-cran-plotmo (3.6.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 27 May 2022 21:32:59 +0200

r-cran-plotmo (3.6.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-cran-formula.

 -- Andreas Tille <tille@debian.org>  Mon, 30 Aug 2021 08:35:10 +0200

r-cran-plotmo (3.6.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 21 Sep 2020 09:51:55 +0200

r-cran-plotmo (3.5.7-2) unstable; urgency=medium

  * Source upload in R 4.0 transition

 -- Andreas Tille <tille@debian.org>  Sat, 16 May 2020 21:47:49 +0200

r-cran-plotmo (3.5.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Sat, 18 Apr 2020 23:22:33 +0200

r-cran-plotmo (3.5.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Trim trailing whitespace.

 -- Dylan Aïssi <daissi@debian.org>  Tue, 29 Oct 2019 11:36:04 +0100

r-cran-plotmo (3.5.5-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Test-Depends: r-cran-rpart

 -- Andreas Tille <tille@debian.org>  Fri, 12 Jul 2019 12:48:36 +0200

r-cran-plotmo (3.5.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Mon, 07 Jan 2019 14:06:38 +0100

r-cran-plotmo (3.5.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Mon, 03 Dec 2018 11:01:41 +0100

r-cran-plotmo (3.5.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.0

 -- Andreas Tille <tille@debian.org>  Fri, 24 Aug 2018 07:52:09 +0200

r-cran-plotmo (3.4.2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 04 Jul 2018 08:28:47 +0200

r-cran-plotmo (3.4.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jun 2018 10:26:36 +0200

r-cran-plotmo (3.4.0-1) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 02 Jun 2018 22:05:12 +0200

r-cran-plotmo (3.3.7-2) unstable; urgency=medium

  * Rebuild for R 3.5 transition

 -- Andreas Tille <tille@debian.org>  Fri, 01 Jun 2018 12:01:12 +0200

r-cran-plotmo (3.3.7-1) unstable; urgency=medium

  * New upstream version
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Sat, 19 May 2018 08:25:35 +0200

r-cran-plotmo (3.3.6-1) unstable; urgency=medium

  * Initial release (closes: #894900)

 -- Andreas Tille <tille@debian.org>  Thu, 05 Apr 2018 13:56:30 +0200
